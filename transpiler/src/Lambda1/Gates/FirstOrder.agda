module Lambda1.Gates.FirstOrder where

-- Lambda1 project imports:
open import Data.PolyTypes using (Tₚ)
open import Lambda1.Gates using (GateLib)

-- Transpiler project imports:
open import Data.PolyTypes.FirstOrder using (⋆U₁; ⟨₁_⟩)
open ⋆U₁ renaming (ty to ⋆ty)

record GateLib₁ : Set₁ where
  field
    Gate  : Set
    ty    : (g : Gate) → ⋆U₁
    ⟦_⟧g  : (g : Gate) → Tₚ (⋆ty ⟨₁ u (ty g) ⟩)

gatelib : GateLib₁ → GateLib
gatelib G = record  { Gate  = GateLib₁.Gate G
                    ; ty    = λ g → ⋆ty (GateLib₁.ty G g)
                    ; ⟦_⟧g  = λ g → GateLib₁.⟦_⟧g G g
                    }

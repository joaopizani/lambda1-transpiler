module Data.PolyTypes.FirstOrder where

open import Data.Star using (map)

-- Lambda 1 project imports:
open import Data.PolyTypes using (Uₚ; Ctxt)
open Uₚ

-- Transpiler project imports:
open import Data.PolyTypes.NonFunction using (U₀)

-- Universe with first order functions:
data U₁ : Uₚ → Set where
  𝟙    : U₁ 𝟙
  _⊗_  : ∀ {σₚ τₚ : Uₚ} → (σ : U₀ σₚ) → (τ : U₀ τₚ) → U₁ (σₚ ⊗ τₚ)
  _⊕_  : ∀ {σₚ τₚ : Uₚ} → (σ : U₀ σₚ) → (τ : U₀ τₚ) → U₁ (σₚ ⊕ τₚ)
  _⇒_  : ∀ {σₚ τₚ : Uₚ} → (σ : U₀ σₚ) → (τ : U₀ τₚ) → U₁ (σₚ ⇒ τₚ)

record ⋆U₁ : Set where
  constructor ⟨₁_⟩
  field
    {ty}  : Uₚ
    u     : U₁ ty

open import Data.Star.Environment ⋆U₁ using (_∋_; lookup) renaming (Ctxt to Ctxt₁; Env to Env₁) public

ctxt : Ctxt₁ → Ctxt
ctxt = map ⋆U₁.ty

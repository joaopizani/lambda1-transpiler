module Data.PolyTypes.Decidable where

open import Function using (_∘_)
open import Relation.Binary using (Decidable)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import Relation.Nullary using (yes; no)

-- Lambda1 project imports:
open import Data.PolyTypes using (Uₚ; 𝟙; _⇒_; _⊗_; _⊕_; Ctxt)

⇒-equality-left : ∀ {u₁ u₂ u₃ u₄} → (u₁ ⇒ u₂) ≡ (u₃ ⇒ u₄) → u₁ ≡ u₃
⇒-equality-left refl = refl

⇒-equality-right : ∀ {u₁ u₂ u₃ u₄} → (u₁ ⇒ u₂) ≡ (u₃ ⇒ u₄) → u₂ ≡ u₄
⇒-equality-right refl = refl

⊗-equality-left : ∀ {u₁ u₂ u₃ u₄} → (u₁ ⊗ u₂) ≡ (u₃ ⊗ u₄) → u₁ ≡ u₃
⊗-equality-left refl = refl

⊗-equality-right : ∀ {u₁ u₂ u₃ u₄} → (u₁ ⊗ u₂) ≡ (u₃ ⊗ u₄) → u₂ ≡ u₄
⊗-equality-right refl = refl

⊕-equality-left : ∀ {u₁ u₂ u₃ u₄} → (u₁ ⊕ u₂) ≡ (u₃ ⊕ u₄) → u₁ ≡ u₃
⊕-equality-left refl = refl

⊕-equality-right : ∀ {u₁ u₂ u₃ u₄} → (u₁ ⊕ u₂) ≡ (u₃ ⊕ u₄) → u₂ ≡ u₄
⊕-equality-right refl = refl

_≟_ : Decidable {A = Uₚ} _≡_
𝟙 ≟ 𝟙 = yes refl
𝟙 ≟ (u₁ ⇒ u₂) = no (λ ())
𝟙 ≟ (u₁ ⊗ u₂) = no (λ ())
𝟙 ≟ (u₁ ⊕ u₂) = no (λ ())
(u₁ ⇒ u₂) ≟ 𝟙 = no (λ ())
(u₁ ⇒ u₂) ≟ (u₃ ⇒ u₄) with u₁ ≟ u₃ | u₂ ≟ u₄
(u₁ ⇒ u₂) ≟ (.u₁ ⇒ .u₂)  | yes refl  | yes refl  = yes refl
...                      | _         | no ¬p     = no (¬p ∘ ⇒-equality-right)
...                      | no ¬p     | _         = no (¬p ∘ ⇒-equality-left)
(u₁ ⇒ u₂) ≟ (u₃ ⊗ u₄) = no (λ ())
(u₁ ⇒ u₂) ≟ (u₃ ⊕ u₄) = no (λ ())
(u₁ ⊗ u₂) ≟ 𝟙 = no (λ ())
(u₁ ⊗ u₂) ≟ (u₃ ⇒ u₄) = no (λ ())
(u₁ ⊗ u₂) ≟ (u₃ ⊗ u₄) with u₁ ≟ u₃ | u₂ ≟ u₄
(u₁ ⊗ u₂) ≟ (.u₁ ⊗ .u₂)  | yes refl  | yes refl  = yes refl
...                      | _         | no ¬p     = no (¬p ∘ ⊗-equality-right)
...                      | no ¬p     | _         = no (¬p ∘ ⊗-equality-left)
(u₁ ⊗ u₂) ≟ (u₃ ⊕ u₄) = no (λ ())
(u₁ ⊕ u₂) ≟ 𝟙 = no (λ ())
(u₁ ⊕ u₂) ≟ (u₃ ⇒ u₄) = no (λ ())
(u₁ ⊕ u₂) ≟ (u₃ ⊗ u₄) = no (λ ())
(u₁ ⊕ u₂) ≟ (u₃ ⊕ u₄) with u₁ ≟ u₃ | u₂ ≟ u₄
(u₁ ⊕ u₂) ≟ (.u₁ ⊕ .u₂)  | yes refl  | yes refl  = yes refl
...                      | _         | no ¬p     = no (¬p ∘ ⊕-equality-right)
...                      | no ¬p     | _         = no (¬p ∘ ⊕-equality-left)

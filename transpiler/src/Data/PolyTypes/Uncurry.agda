module Data.PolyTypes.Uncurry where

open import Data.List using (List; []; _∷_)
open import Data.Product using (_×_; _,_)
open import Data.Star using (map)

-- Lambda 1 project imports:
open import Data.PolyTypes using (Uₚ; Ctxt)
open Uₚ

-- Transpiler project imports:
open import Data.PolyTypes.NonFunction using (U₀; ⋆U₀; ⟨₀_⟩)
open U₀

-- Universe of low order types:
data Uᵤ : Uₚ → Set where
  𝟙    : Uᵤ 𝟙
  _⊗_  : ∀ {σₚ τₚ : Uₚ} → (σ : U₀ σₚ) → (τ : U₀ τₚ) → Uᵤ (σₚ ⊗ τₚ)
  _⊕_  : ∀ {σₚ τₚ : Uₚ} → (σ : U₀ σₚ) → (τ : U₀ τₚ) → Uᵤ (σₚ ⊕ τₚ)
  _⇒_  : ∀ {σₚ τₚ : Uₚ} → (σ : U₀ σₚ) → (τs : Uᵤ τₚ) → Uᵤ (σₚ ⇒ τₚ)

_⇛_ : ∀ {σₚ τₚ : Uₚ} → U₀ σₚ → U₀ τₚ → Uᵤ (σₚ ⇒ τₚ)
σ ⇛ τ with ⋆U₀.u ⟨₀ τ ⟩
... | 𝟙        = σ ⇒ 𝟙
... | σ₀ ⊗ τ₀  = σ ⇒ (σ₀ ⊗ τ₀)
... | σ₀ ⊕ τ₀  = σ ⇒ (σ₀ ⊕ τ₀)

record ⋆Uᵤ : Set where
  constructor ⟨ᵤ_⟩
  field
    {ty}  : Uₚ
    u     : Uᵤ ty

uncurry : ⋆Uᵤ → (List ⋆U₀ × ⋆U₀)
uncurry ⟨ᵤ 𝟙 ⟩      = [] , ⟨₀ 𝟙 ⟩
uncurry ⟨ᵤ σ ⊗ τ ⟩  = [] , ⟨₀ σ ⊗ τ ⟩
uncurry ⟨ᵤ σ ⊕ τ ⟩  = [] , ⟨₀ σ ⊕ τ ⟩
uncurry ⟨ᵤ σ ⇒ τs ⟩ with uncurry ⟨ᵤ τs ⟩
... | τᵢs , τₒ = ⟨₀ σ ⟩ ∷ τᵢs , τₒ

open import Data.Star.Environment ⋆Uᵤ using (_∋_; lookup) renaming (Ctxt to Ctxtᵤ; Env to Envᵤ) public

ctxt : Ctxtᵤ → Ctxt
ctxt = map ⋆Uᵤ.ty

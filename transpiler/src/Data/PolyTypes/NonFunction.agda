module Data.PolyTypes.NonFunction where

open import Data.Star using (map)

-- Lambda 1 project imports:
open import Data.PolyTypes using (Uₚ; Ctxt)
open Uₚ

-- Universe without functions:
data U₀ : Uₚ → Set where
  𝟙    : U₀ 𝟙
  _⊗_  : ∀ {σₚ τₚ : Uₚ} → (σ : U₀ σₚ) → (τ : U₀ τₚ) → U₀ (σₚ ⊗ τₚ)
  _⊕_  : ∀ {σₚ τₚ : Uₚ} → (σ : U₀ σₚ) → (τ : U₀ τₚ) → U₀ (σₚ ⊕ τₚ)

record ⋆U₀ : Set where
  constructor ⟨₀_⟩
  field
    {ty}  : Uₚ
    u     : U₀ ty

open import Data.Star.Environment ⋆U₀ using (_∋_; lookup) renaming (Ctxt to Ctxt₀; Env to Env₀) public

ctxt : Ctxt₀ → Ctxt
ctxt = map ⋆U₀.ty

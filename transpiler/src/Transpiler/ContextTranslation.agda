module Transpiler.ContextTranslation where

open import Data.Nat using (ℕ)
open import Data.Star using (map)
open import Data.Product using (_×_)

-- Transpiler project imports:
open import Data.PolyTypes.FirstOrder using (Ctxt₁)
open import Transpiler.DataTranslation using (io)

-- Public
open import Data.Star.Environment (ℕ × ℕ) renaming
  ( Ctxt    to Ctxt½
  ; _∋_     to _∋½_
  ; Env     to Env½
  ; lookup  to lookup½
  ) public

open import Data.Product renaming
  ( proj₁ to i½
  ; proj₂ to o½
  ) public

convert-context : Ctxt₁ → Ctxt½
convert-context = Data.Star.map io

module Transpiler.GateTranslation where

-- Piware project imports:
open import PiWare.Circuit.Algebra using (TyGate)
open import PiWare.Gates using (Gates)

-- Transpiler project imports:
open import Data.PolyTypes.FirstOrder
open import Lambda1.Gates.FirstOrder
open import Transpiler.DataTranslation

convert-gate-lib : GateLib₁ → Gates
convert-gate-lib G = record  { Gate#  = GateLib₁.Gate G
                             ; #in    = λ g → |i| (GateLib₁.ty G g)
                             ; #out   = λ g → |o| (GateLib₁.ty G g)
                             }

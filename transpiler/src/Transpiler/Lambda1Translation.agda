module Transpiler.Lambda1Translation where

open import Data.Nat using (_+_)
open import Data.Product using (proj₁; proj₂)
open import Data.Star using (ε; _◅_)

-- Lambda1 project imports:
open import Data.PolyTypes using (Uₚ)
open Uₚ
open import Lambda1.Lambda1 using (λ₁)
open λ₁

-- PiWare project imports:
open import PiWare.Circuit using (ℂ[_]; IsComb)
open ℂ[_]

-- Transpiler project imports:
open import Data.PolyTypes.FirstOrder
open ⋆U₁
open import Lambda1.Gates.FirstOrder
open import Transpiler.CircuitIntermediateLanguage
open import Transpiler.ContextTranslation
open import Transpiler.DataTranslation
open import Transpiler.GateTranslation

lambda : ∀ {G Γ σ i o} → ℂσ½[ G ] (io σ ◅ Γ) i o → ℂσ½[ G ] Γ (? + i) o
lambda = {!   !}

transpile½ :
    (G : GateLib₁) →
    (Γ : Ctxt₁) →
    (τ : ⋆U₁) →
    λ₁ ⦃ gatelib G ⦄ (ctxt Γ) (ty τ) →
    ℂσ½[ convert-gate-lib G ] (convert-context Γ) (|i| τ) (|o| τ)
transpile½ G Γ τ           ⟨ g ⟩                      = {!   !}
transpile½ G Γ τ           #[ i ]                     = {!   !}
transpile½ G Γ τ           (f ＄ x)                    = {!   !}
transpile½ G Γ τ           (letₓ x inₑ e)             = {!   !}
transpile½ G Γ τ           (loop x)                   = {!   !}
transpile½ G Γ ⟨₁ σ ⊗ τ ⟩  (x ， y)                    = {!   !}
transpile½ G Γ τ           (case⊗ x of b)             = {!   !}
transpile½ G Γ ⟨₁ σ ⊕ τ ⟩  (inl x)                    = {!   !}
transpile½ G Γ ⟨₁ σ ⊕ τ ⟩  (inr x)                    = {!   !}
transpile½ G Γ τ           (case⊕ x either x₁ or x₂)  = {!   !}

transpile :
    (G : GateLib₁) →
    (τ : ⋆U₁) →
    λ₁ ⦃ gatelib G ⦄ ε (ty τ) →
    ℂ[ convert-gate-lib G ] {IsComb.σ} (|i| τ) (|o| τ)
transpile G τ x = closed (transpile½ G ε τ x)

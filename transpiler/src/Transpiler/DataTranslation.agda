module Transpiler.DataTranslation where

open import Data.Bool using (true; false)
open import Data.Nat using (ℕ; _+_; _⊔_)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Function using (_∘_)
open import Relation.Nullary.Decidable using (⌊_⌋)

-- Lambda1 project imports:
open import Data.PolyTypes using (𝟙; _⇒_; _⊗_; _⊕_)

-- Transpiler project imports:
open import Data.PolyTypes.Decidable using (_≟_)
open import Data.PolyTypes.FirstOrder using (U₁; ⋆U₁; ⟨₁_⟩)
open U₁
open ⋆U₁
open import Data.PolyTypes.NonFunction using (U₀; ⋆U₀; ⟨₀_⟩)
open U₀
open ⋆U₀

num-wires : ⋆U₀ → ℕ
num-wires ⟨₀ 𝟙     ⟩  = 0
num-wires ⟨₀ σ ⊗ τ ⟩  = num-wires ⟨₀ σ ⟩ + num-wires ⟨₀ τ ⟩
num-wires ⟨₀ σ ⊕ τ ⟩  = 1 + (num-wires ⟨₀ σ ⟩ ⊔ num-wires ⟨₀ τ ⟩)

io : ⋆U₁ → ℕ × ℕ
io ⟨₁ 𝟙     ⟩  = 0 , num-wires ⟨₀ 𝟙 ⟩
io ⟨₁ σ ⊗ τ ⟩  = 0 , num-wires ⟨₀ σ ⊗ τ ⟩
io ⟨₁ σ ⊕ τ ⟩  = 0 , num-wires ⟨₀ σ ⊕ τ ⟩
io ⟨₁ σ ⇒ τ ⟩  = num-wires ⟨₀ σ ⟩ , num-wires ⟨₀ τ ⟩

|i| : ⋆U₁ → ℕ
|i| = proj₁ ∘ io

|o| : ⋆U₁ → ℕ
|o| = proj₂ ∘ io

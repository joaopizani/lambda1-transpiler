module Transpiler.AtomTranslation where

open import Data.Nat.Base using (ℕ)

-- Lambda1 project imports:
open import Data.PolyTypes using (Tₚ)

-- PiWare project imports:
open import Data.Serializable using (_⇕_)
open import PiWare.Atomic using (Atomic)
open Atomic using (Atom)

-- Transpiler project imports:
open import Data.PolyTypes.FirstOrder

-- TODO translate Uₚ⊣ to chosen Atomic
record TranslatableAtoms : Set₁ where
  field
    piware-atomic  : Atomic
    vec-size       : ℕ
    serializer     : (_⇕_) (Tₚ {!   !}) (Atom piware-atomic) {vec-size}

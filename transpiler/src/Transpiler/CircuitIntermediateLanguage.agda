module Transpiler.CircuitIntermediateLanguage where

open import Data.Nat using (_+_)
open import Data.Star using (ε; _◅_)

-- PiWare project imports:
open import PiWare.Circuit using (ℂ[_]; IsComb; σ)
open ℂ[_]
open import PiWare.Gates using (Gates)
open Gates using (#in; #out)
open import PiWare.Interface using (Ix)
open import PiWare.Plugs.Core using (_⤪_)

-- Transpiler project imports:
open import Transpiler.ContextTranslation using (Ctxt½; _∋½_; i½; o½)

data ℂσ½[_] (G : Gates) : Ctxt½ → Ix → Ix → Set where
    Gate  : ∀ {Γ g#} → ℂσ½[ G ] Γ (#in G g#) (#out G g#)
    Plug  : ∀ {Γ i o} → i ⤪ o → ℂσ½[ G ] Γ i o
    _⟫_   : ∀ {Γ i m o} → ℂσ½[ G ] Γ i m → ℂσ½[ G ] Γ m o → ℂσ½[ G ] Γ i o
    _∥_   : ∀ {Γ i₁ o₁ i₂ o₂} → ℂσ½[ G ] Γ i₁ o₁ → ℂσ½[ G ] Γ i₂ o₂ → ℂσ½[ G ] Γ (i₁ + i₂) (o₁ + o₂)
    #[_]  : ∀ {Γ τ} → Γ ∋½ τ → ℂσ½[ G ] Γ (i½ τ) (o½ τ)

closed : ∀ {G : Gates} {i o : Ix} → ℂσ½[ G ] ε i o → ℂ[ G ] {σ} i o
closed (Gate {g# = g#})  = Gate g#
closed (Plug x)          = Plug x
closed (c ⟫ c₁)          = closed c ⟫ closed c₁
closed (c ∥ c₁)          = closed c ∥ closed c₁
closed #[ () ◅ _ ]
